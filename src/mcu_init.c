
#include <msp430g2153.h>
#include "mcu_init.h"

void clkConfig();

/*
 * Init MCU peripherals
 */
void mcuInit(){
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    clkConfig();

}
/*
 * Configure clocks
 */
void clkConfig(){

    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    //SET LFXT1 to the VLO @ 12khz
    BCSCTL3 |= LFXT1S_2;

}
